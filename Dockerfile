FROM openjdk:8-jdk-alpine
RUN addgroup -S sravyadatla && adduser -S sravyadatla -G sravyadatla
USER sravyadatla:sravyadatla
ARG JAR_FILE=build/libs/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]